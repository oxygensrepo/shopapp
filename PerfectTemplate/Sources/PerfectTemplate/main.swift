//
//  main.swift
//  PerfectTemplate
//
//  Created by Kyle Jessup on 2015-11-05.
//	Copyright (C) 2015 PerfectlySoft, Inc.

import PerfectHTTP
import PerfectHTTPServer

let server = HTTPServer()
let profileController = ProfileController()
let authController = AuthController()
var routes = Routes()

routes.add(method: .post, uri: "/register", handler: profileController.register)
routes.add(method: .post, uri: "/changeUserData", handler: profileController.updateProfile)
routes.add(method: .post, uri: "/login", handler: authController.login)
routes.add(method: .post, uri: "/logout", handler: authController.logout)


server.addRoutes(routes)
server.serverPort = 8080

do {
    try server.start()
} catch {
    fatalError("Network error - \(error)")
}
