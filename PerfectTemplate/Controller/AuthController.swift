//
//  AuthController.swift
//  PerfectTemplate
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import PerfectHTTP

class AuthController {
    
    let login: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Incorrect user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let loginRequest = LoginRequest(json)
            if  (loginRequest.username == "geekbrains" && loginRequest.password == "admin") {
                try response.setBody(json: ["result": 1, "user": ["id": 123, "login": "geekbrains", "name": "John", "lastname": "Doe"]])
            } else {
                try response.setBody(json: ["result": 0, "errorMessage": "Incorrect data"])
            }
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let logout: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Incorrect user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let logoutRequest = LogoutRequest(json)
            if  (logoutRequest.id_user == 123) {
                try response.setBody(json: ["result": 1])
            } else {
                try response.setBody(json: ["result": 0, "errorMessage": "Incorrect data"])
            }
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
}

