//
//  ProfileController.swift
//  PerfectTemplate
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import PerfectHTTP

class ProfileController {
    
    let register: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Incorrect user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let registerRequest = ProfileRequest(json)
            if  (!registerRequest.username.isEmpty &&
                !registerRequest.password.isEmpty &&
                !registerRequest.email.isEmpty &&
                registerRequest.id_user != 123) {
                try response.setBody(json: ["result": 1, "userMessage": "Регистрация прошла успешно!"])
            } else {
                try response.setBody(json: ["result": 0, "errorMessage": "Incorrect or missing data"])
            }
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let updateProfile: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Incorrect user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let registerRequest = ProfileRequest(json)
            if  (registerRequest.id_user == 123) {
                try response.setBody(json: ["result": 1])
            } else {
                try response.setBody(json: ["result": 0, "errorMessage": "Incorrect or missing data"])
            }
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
}
