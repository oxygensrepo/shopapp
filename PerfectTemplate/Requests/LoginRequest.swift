//
//  LoginRequest.swift
//  PerfectTemplate
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

struct LoginRequest {
    
    var username: String = ""
    var password: String = ""
    
    init(_ json: [String: AnyObject]) {
        if let username = json["username"] as? String {
            self.username = username
        }
        if let password = json["password"] as? String {
            self.password = password
        }
    }
}

