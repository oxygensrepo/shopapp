//
//  LogoutRequest.swift
//  PerfectTemplate
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

struct LogoutRequest {
    
    var id_user: Int = 0
    
    init(_ json: [String: AnyObject]) {
        if let id_user = json["id_user"] as? Int {
            self.id_user = id_user
        }
    }
}
