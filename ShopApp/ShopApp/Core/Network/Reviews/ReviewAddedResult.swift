//
//  ReviewAddedResult.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 04/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

struct ReviewAddedResult: Codable {
    let result: Int
    let userMessage: String
}
