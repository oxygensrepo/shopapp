//
//  ReviewRequestFactory.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 04/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

protocol ReviewRequestFactory {
    
    func addReview(userId: Int,
                   commentText: String,
                   completionHandler: @escaping (DataResponse<ReviewAddedResult>) -> Void);
    
    func approveReview(commentId: Int,
                       completionHandler: @escaping (DataResponse<ReviewUpdatedResult>) -> Void);
    
    func removeReview(commentId: Int,
                      completionHandler: @escaping (DataResponse<ReviewUpdatedResult>) -> Void);
    
}
