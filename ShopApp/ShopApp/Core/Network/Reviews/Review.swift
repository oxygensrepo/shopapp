//
//  Review.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 04/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

class Review: AbstractRequestFactory {
    
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl: URL
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
        self.baseUrl = DefaultDataRequest().baseUrl
    }
}

extension Review: ReviewRequestFactory {
    
    func addReview(userId: Int,
                   commentText: String,
                   completionHandler: @escaping (DataResponse<ReviewAddedResult>) -> Void){
        let requestModel = AddReview(baseUrl: baseUrl,
                                     userId: userId,
                                     commentText: commentText);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
    
    func approveReview(commentId: Int,
                       completionHandler: @escaping (DataResponse<ReviewUpdatedResult>) -> Void){
        let requestModel = ApproveReview(baseUrl: baseUrl,
                                         commentId: commentId);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
    
    func removeReview(commentId: Int,
                      completionHandler: @escaping (DataResponse<ReviewUpdatedResult>) -> Void){
        let requestModel = RemoveReview(baseUrl: baseUrl,
                                        commentId: commentId);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
}

extension Review {
    
    struct AddReview: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "addReview.json"
        let userId: Int
        let commentText: String
        
        var parameters: Parameters? {
            return [
                "id_user" : userId,
                "text" : commentText
            ]
        }
    }
    
    struct ApproveReview: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "approveReview.json"
        let commentId: Int
        
        var parameters: Parameters? {
            return [
                "id_comment" : commentId
            ]
        }
    }
    
    struct RemoveReview: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "removeReview.json"
        let commentId: Int
        
        var parameters: Parameters? {
            return [
                "id_comment" : commentId
            ]
        }
    }
    
}
