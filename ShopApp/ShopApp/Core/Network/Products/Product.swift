//
//  Product.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

struct Product: Codable {
    let id_product: UInt
    let product_name: String
    let price: UInt
}
