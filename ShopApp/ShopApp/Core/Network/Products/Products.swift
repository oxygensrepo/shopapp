//
//  Products.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

class Products: AbstractRequestFactory {
    
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl: URL
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
        self.baseUrl = DefaultDataRequest().baseUrl
    }
}

extension Products: ProductsRequestFactory {
    
    func getProductsList(pageNumber: Int,
                         categoryId: Int,
                         completionHandler: @escaping (DataResponse<[Product]>) -> Void){
        let requestModel = Products(baseUrl: baseUrl,
                                    pageNumber: pageNumber,
                                    categoryId: categoryId);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
    
}

extension Products {
    
    struct Products: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "catalogData.json"        
        let pageNumber: Int
        let categoryId: Int
        
        var parameters: Parameters? {
            return [
                "page_number" : pageNumber,
                "id_category" : categoryId
            ]
        }
    }
    

}

