//
//  ProductsRequestFactory.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

protocol ProductsRequestFactory {
    
    func getProductsList(
        pageNumber: Int,
        categoryId: Int,
        completionHandler: @escaping (DataResponse<[Product]>) -> Void);
    
}
