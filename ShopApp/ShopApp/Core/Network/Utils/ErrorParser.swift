//
//  ErrorParser.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 23/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

class ErrorParser: AbstractErrorParser {
    
    func parse(_ result: Error) -> Error {
        return result
    }
    
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }
}
