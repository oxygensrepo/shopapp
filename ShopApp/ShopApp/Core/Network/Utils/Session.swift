//
//  Session.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 17/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

class Session {
    
    var guest: User = User(id: -1,
                           login: Users.GUEST.rawValue,
                           name: Users.GUEST.rawValue,
                           lastname: Users.GUEST.rawValue)
    
    var user: User
    
    init(){
        user = guest
    }
    
    public static let shared = Session()
    
}
