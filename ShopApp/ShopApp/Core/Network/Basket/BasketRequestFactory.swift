//
//  BasketRequestFactory.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 11/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

protocol BasketRequestFactory {
    
    func addToBasket(productId: Int,
                     quantity: Int,
                     completionHandler: @escaping (DataResponse<BasketUpdatedResult>) -> Void);
    
    func removeFromBasket(productId: Int,
                          completionHandler: @escaping (DataResponse<BasketUpdatedResult>) -> Void);
    
    
    func getBasket(userId: Int,
                   completionHandler: @escaping (DataResponse<BasketResult>) -> Void);
    
}
