//
//  Basket.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 11/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

struct Basket: Codable {
    
    let productId: Int
    let productName: String
    let productPrice: UInt
    let productQuantity: Int

    enum CodingKeys: String, CodingKey {
        case productId = "id_product"
        case productName = "product_name"
        case productPrice = "price"
        case productQuantity = "quantity"
    }
}
