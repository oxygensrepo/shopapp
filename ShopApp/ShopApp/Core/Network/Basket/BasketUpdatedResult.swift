//
//  BasketUpdatedResult.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 11/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

struct BasketUpdatedResult: Codable {
    let result: Int
}
