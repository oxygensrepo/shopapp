//
//  Baskets.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 11/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

class Baskets: AbstractRequestFactory {
    
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl: URL
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
        self.baseUrl = DefaultDataRequest().baseUrl
    }
}

extension Baskets: BasketRequestFactory {
    
    func addToBasket(productId: Int,
                     quantity: Int,
                     completionHandler: @escaping (DataResponse<BasketUpdatedResult>) -> Void) {
        let requestModel = AddToBasket(baseUrl: baseUrl,
                                       productId: productId,
                                       quantity: quantity);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
    
    func removeFromBasket(productId: Int,
                          completionHandler: @escaping (DataResponse<BasketUpdatedResult>) -> Void) {
        let requestModel = RemoveFromBasket(baseUrl: baseUrl,
                                            productId: productId);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
    
    func getBasket(userId: Int,
                   completionHandler: @escaping (DataResponse<BasketResult>) -> Void) {
        let requestModel = GetBasket(baseUrl: baseUrl,
                                     userId: userId);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
    
}

extension Baskets {
    
    struct AddToBasket: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "addToBasket.json"
        let productId: Int
        let quantity: Int

        var parameters: Parameters? {
            return [
                "id_product" : productId,
                "quantity" : quantity
            ]
        }
    }
    
    struct RemoveFromBasket: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "deleteFromBasket.json"
        let productId: Int
        
        var parameters: Parameters? {
            return [
                "id_product" : productId
            ]
        }
    }
    
    struct GetBasket: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "getBasket.json"
        let userId: Int
        
        var parameters: Parameters? {
            return [
                "id_user" : userId
            ]
        }
    }
    
}
