//
//  UserUpdatedResult.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 23/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

struct UserUpdatedResult: Codable {
    let result: Int
}
