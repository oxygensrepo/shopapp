//
//  UserRequestFactory.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 23/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

protocol UserRequestFactory {
    
    func registerUser(userId: Int,
                      userName: String,
                      password: String,
                      email: String,
                      gender: String,
                      creditCard: String,
                      bio: String,
                      completionHandler: @escaping (DataResponse<UserRegisteredResult>) -> Void);

    func updateUser(userId: Int,
                    userName: String,
                    password: String,
                    email: String,
                    gender: String,
                    creditCard: String,
                    bio: String,
                    completionHandler: @escaping (DataResponse<UserUpdatedResult>) -> Void);
}
