//
//  UserProfile.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 23/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

class UserProfile: AbstractRequestFactory {
    
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl: URL
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
        self.baseUrl = DefaultDataRequest().baseUrl
    }
}

extension UserProfile: UserRequestFactory {
    
    func registerUser(userId: Int,
                      userName: String,
                      password: String,
                      email: String,
                      gender: String,
                      creditCard: String,
                      bio: String,
                      completionHandler: @escaping (DataResponse<UserRegisteredResult>) -> Void){
        let requestModel = RegisterUser(baseUrl: baseUrl,
                                        userId: userId,
                                        userName: userName,
                                        password: password,
                                        email: email,
                                        gender: gender,
                                        creditCard: creditCard,
                                        bio: bio);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
    
    func updateUser(userId: Int,
                    userName: String,
                    password: String,
                    email: String,
                    gender: String,
                    creditCard: String,
                    bio: String,
                    completionHandler: @escaping (DataResponse<UserUpdatedResult>) -> Void){
        let requestModel = UpdateUser(baseUrl: baseUrl,
                                      userId: userId,
                                      userName: userName,
                                      password: password,
                                      email: email,
                                      gender: gender,
                                      creditCard: creditCard,
                                      bio: bio);
        self.request(request: requestModel, completionHandler: completionHandler);
    };
}

extension UserProfile {
    
    struct RegisterUser: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "registerUser.json"
        let userId: Int
        let userName: String
        let password: String
        let email: String
        let gender: String
        let creditCard: String
        let bio: String
        
        var parameters: Parameters? {
            return [
                "id_user" : userId,
                "username" : userName,
                "password" : password,
                "email" : email,
                "gender": gender,
                "credit_card" : creditCard,
                "bio" : bio
            ]
        }
    }
    
    struct UpdateUser: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "changeUserData.json"        
        let userId: Int
        let userName: String
        let password: String
        let email: String
        let gender: String
        let creditCard: String
        let bio: String
        
        var parameters: Parameters? {
            return [
                "id_user" : userId,
                "username" : userName,
                "password" : password,
                "email" : email,
                "gender": gender,
                "credit_card" : creditCard,
                "bio" : bio
            ]
        }
    }
}
