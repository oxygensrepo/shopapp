//
//  AuthRequestFactory..swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 23/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Alamofire

protocol AuthRequestFactory {
    
    func login(userName: String,
               password: String,
               completionHandler: @escaping (DataResponse<LoginResult>) -> Void);

    func logout(userId: Int,
                completionHandler: @escaping (DataResponse<LogoutResult>) -> Void);
    
}
