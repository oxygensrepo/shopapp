//
//  LoginResult.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 23/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

struct LoginResult: Codable {
    let result: Int
    let user: User
}
