//
//  ProductInfoController.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 23/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

class ProductInfoController: UIViewController, Alertable {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var reviewText: UITextView!
    
    @IBAction func sendReviewButton(_ sender: Any) {
        if !reviewText.text.isEmpty {
            sendReview(profileId: Session.shared.user.id,
                       comment: reviewText.text)
        }
    }
    
    @IBAction func addToBascketButton(_ sender: Any) {
        if Int(product!.id_product) > 0 {
            addToBasket(productId: Int(product!.id_product), quantity: 1)
        }
    }
    
    var product: Product!
    let reviewFactory = RequestFactory().makeReviewsRequestFatory()
    let basketFactory = RequestFactory().makeBasketRequestFatory()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.title = Titles.PRODUCT.rawValue
    }
    
    func configureView() -> Void {
        if product != nil {
            productName.text = product.product_name
            productImage.image = UIImage(named: "apple.png")
            productPrice.text = String(product.price)
        }
    }
    
    
}
