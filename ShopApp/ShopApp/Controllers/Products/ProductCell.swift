//
//  ProductCell.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 18/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    public func configure(with product: Product) {
        productName.text = String(product.product_name)
        productPrice.text = String(product.price)
    }

}
