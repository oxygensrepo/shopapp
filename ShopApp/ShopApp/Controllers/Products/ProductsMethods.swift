//
//  ProductsMethods.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 18/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

extension ProductsController {

    func loadProductsData(pageNumber: Int, categoryId: Int) {
        productFactory.getProductsList(pageNumber: pageNumber,
                                       categoryId: categoryId) {response in
                                            switch response.result {
                                                case .success(let data):
                                                    DispatchQueue.main.async{
                                                        self.catalog = data
                                                        self.tableView?.reloadData()
                                                    }
                                                case .failure(let error):
                                                    self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
                                           }
        }
    }
    
    @objc func handlerRefresh(){
        loadProductsData(pageNumber: currentPage, categoryId: currentCategoryId)
        refresher.endRefreshing()
    }

    func addRefresher(){
        refresher = UIRefreshControl()
        tableView.addSubview(refresher)
        refresher.attributedTitle = NSAttributedString(string: Messages.PULL_REFRESH.rawValue)
        refresher.tintColor = UIColor.purple
        refresher.addTarget(self, action: #selector(handlerRefresh), for: .valueChanged)
    }
    
}

