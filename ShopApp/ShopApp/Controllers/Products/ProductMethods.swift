//
//  ProductMethods.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 26/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

extension ProductInfoController {
    
    func sendReview(profileId: Int, comment: String){
        reviewFactory.addReview(userId: profileId,
                                commentText: comment) {response in
                                    switch response.result {
                                        case .success(_):
                                            DispatchQueue.main.async{
                                                self.showAlertMsg(title: Titles.CONFIRMATION.rawValue,
                                                                  message: Messages.DATA_SAVED.rawValue,
                                                                  timeSeconds: 2)
                                        }
                                        case .failure(let error):
                                            self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
                                    }
        }
    }
    
    func approveReview(commentId: Int){
        reviewFactory.approveReview(commentId: commentId) {response in
                                    switch response.result {
                                    case .success(_):
                                        DispatchQueue.main.async{
                                            self.performAlert(title: Titles.CONFIRMATION.rawValue,
                                                              message: Messages.REVIEW_APPROVED.rawValue)
                                        }
                                    case .failure(let error):
                                        self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
                                    }
        }
    }
    
    func removeReview(commentId: Int){
        reviewFactory.removeReview(commentId: commentId) {response in
            switch response.result {
            case .success(_):
                DispatchQueue.main.async{
                    self.reviewText.text = ""
                }
            case .failure(let error):
                self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
            }
        }
    }
    
    func addToBasket(productId: Int, quantity: Int){
        basketFactory.addToBasket(productId: productId,
                                  quantity: quantity) {response in
            switch response.result {
                case .success(_):
                    DispatchQueue.main.async{
                        self.showAlertMsg(title: Titles.CONFIRMATION.rawValue,
                                          message: Messages.ADD_BASKET_SUCCESS.rawValue,
                                          timeSeconds: 2)
                    }
                case .failure(let error):
                    self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
            }
        }
    }

}
