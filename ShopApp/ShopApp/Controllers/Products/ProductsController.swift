//
//  ProductsController.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 18/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

class ProductsController: UITableViewController, Alertable {

    @IBOutlet weak var table: UITableView!
    @IBAction func previousPage(_ sender: Any) {
        if (currentPage > 1) {
            currentPage -= 1
            loadProductsData(pageNumber: currentPage, categoryId: currentCategoryId)
        }
    }
    
    @IBAction func nextPage(_ sender: Any) {
        currentPage += 1
        loadProductsData(pageNumber: currentPage, categoryId: currentCategoryId)
    }
    
    let productFactory = RequestFactory().makeProductsRequestFatory()
    var refresher: UIRefreshControl!
    var catalog: [Product] = []
    var currentPage: Int = 1
    var currentCategoryId: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadProductsData(pageNumber: currentPage, categoryId: currentCategoryId)
        addRefresher()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catalog.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellNames.productCell.rawValue, for: indexPath) as! ProductCell
        cell.configure(with: catalog[indexPath.row])
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextScene =  segue.destination as? ProductInfoController {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let selectedProduct = catalog[indexPath.row]
                nextScene.product = selectedProduct
            }
        }
    }
    
}
