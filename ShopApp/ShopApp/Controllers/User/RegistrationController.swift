//
//  RegistrationController.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 19/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit
import Foundation

class RegistrationController: UIViewController, Alertable, TrackableMixin {
    
    let profileFactory = RequestFactory().makeProfileRequestFatory()

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userCreditCard: UITextField!
    @IBOutlet weak var userBio: UITextView!
    
    var userGender: String = Genders.MALE.rawValue
   
    @IBAction func userGenderButton(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
            case 0:
                userGender = Genders.MALE.rawValue
            case 1:
                userGender = Genders.FEMALE.rawValue
            default:
                break;
        }
    }
    
    @IBAction func submitButton(_ sender: Any) {
        registration (userName: userName.text!,
                      userPassword: userPassword.text!,
                      userGender: userGender,
                      userEmail: userEmail.text!,
                      userCreditCard: userCreditCard.text!,
                      userBio: userBio.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userPassword.isSecureTextEntry = true
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.title = Titles.REGISTRATION_FORM.rawValue
    }

}
