//
//  UserProfileMethods.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 19/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

extension UserProfileController {

    func updateUserProfile(profileId: Int,
                           profileName: String,
                           profilePassword: String,
                           profileGender: String,
                           profileEmail: String,
                           profileCreditCard: String,
                           profileBio: String){
        if (!profileName.isEmpty &&
            !profilePassword.isEmpty &&
            !profileEmail.isEmpty) {
            profileFactory.updateUser(userId: profileId,
                                      userName: profileName,
                                      password: profilePassword,
                                      email: profileEmail,
                                      gender: profileGender,
                                      creditCard: profileCreditCard,
                                      bio: profileBio) {response in
                                            switch response.result{
                                            case .success(_):
                                                DispatchQueue.main.async{
                                                    self.track(AnalyticsEvent.updateProfile(
                                                        name: AnalyticsEvent.UpdateProfileParams.nameDefault)
                                                    )
                                                    self.performAlert(title: Titles.CONFIRMATION.rawValue,
                                                                      message: Messages.DATA_SAVED.rawValue)
                                                }
                                            case .failure(let error):
                                                self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
                                            }
            }
        } else {
            self.performAlert(title: Titles.ERROR.rawValue,
                              message: Messages.DATA_INCOMPLETE.rawValue)
        }
    }
    
}
