//
//  RegistrationMethods.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 19/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

extension RegistrationController {

    func registration (userName: String,
                       userPassword: String,
                       userGender: String,
                       userEmail: String,
                       userCreditCard: String,
                       userBio: String){
        if (!userName.isEmpty &&
            !userPassword.isEmpty &&
            !userEmail.isEmpty) {
            profileFactory.registerUser(userId: -9999,
                                        userName: userName,
                                        password: userPassword,
                                        email: userEmail,
                                        gender: userGender,
                                        creditCard: userCreditCard,
                                        bio: userBio) {response in
                                            switch response.result{
                                            case .success(_):
                                                DispatchQueue.main.async{
                                                    self.track(AnalyticsEvent.registration(
                                                        name: AnalyticsEvent.RegistrationParams.nameDefault,
                                                        parameter: AnalyticsEvent.RegistrationParams.parameterDefault)
                                                    )
                                                    self.performSegue(withIdentifier: SeguesId.goToAuth.rawValue,
                                                                      sender: nil)
                                                    self.performAlert(title: Titles.CONFIRMATION.rawValue,
                                                                      message: Messages.REGISTRATION_SUCCESS.rawValue)
                                                }
                                            case .failure(let error):
                                                self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
                                            }
                                        }
        } else {
            self.performAlert(title: Titles.ERROR.rawValue, message: Messages.DATA_INCOMPLETE.rawValue)
        }
    }
    
}

