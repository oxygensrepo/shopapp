//
//  ViewController.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 18/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

class LoginController: UIViewController, Alertable, Closeable, TrackableMixin {

    let authFactory = RequestFactory().makeAuthRequestFatory()
    
    @IBOutlet weak var loginPageScroll: UIScrollView!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func loginButton(_ sender: Any) {
        authorization(userLogin: loginTextField.text!,
                      userPassword: passwordTextField.text!)
    }
    
    @IBAction func registrationButton(_ sender: Any) {
    }
    
    @IBAction func logOut(_ segue: UIStoryboardSegue){
        cleanFields()
        logout(userId: Session.shared.user.id)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addHideKeyboardGesture()
        passwordTextField.isSecureTextEntry = true
    }    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribeToNotification()        
    }


}

