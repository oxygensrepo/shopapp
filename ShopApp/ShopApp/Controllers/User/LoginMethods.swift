//
//  LoginMethods.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 17/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

extension LoginController {
    
    func authorization(userLogin: String, userPassword: String){
        if (!userLogin.isEmpty && !userPassword.isEmpty){
            authFactory.login(userName: userLogin,
                              password: userPassword) {response in
                              switch response.result{
                                case .success(let data):
                                    Session.shared.user = data.user
                                    DispatchQueue.main.async{
                                        self.track(AnalyticsEvent.login(
                                            method: AnalyticsEvent.LoginParams.methodDefault,
                                            success: true)
                                            )
                                        self.performSegue(withIdentifier: SeguesId.goToStore.rawValue,
                                                          sender: nil)
                                    }
                                case .failure(let error):
                                    DispatchQueue.main.async{
                                        self.assertionFailure(Messages.LOGIN_FAILED.rawValue + error.localizedDescription)
                                    }
                                }
            }
        } else {
            self.performAlert(title: Titles.ERROR.rawValue,
                              message: Messages.LOGIN_FAILED.rawValue)
        }
    }
    
    @objc func hideKeyboard(){
        self.loginPageScroll.endEditing(true)
    }
    
    @objc func keyboardWasShown(notification: Notification){
        let info = notification.userInfo! as NSDictionary
        let kbSize = (info.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: kbSize.height + 8.0, right: 0.0)
        self.loginPageScroll.contentInset = contentInsets
        loginPageScroll.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillBeHidden(notification: Notification){
        let contentInsets = UIEdgeInsets.zero
        loginPageScroll.contentInset = contentInsets
        loginPageScroll.scrollIndicatorInsets = contentInsets
    }
    
    func addHideKeyboardGesture(){
        let hideKeyboardGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(self.hideKeyboard))
        loginPageScroll.addGestureRecognizer(hideKeyboardGesture)
    }
    
    func subscribeToNotification(){
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWasShown),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.keyboardWillBeHidden(notification: )),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func unsubscribeFromNotification(){
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func cleanFields(){
        loginTextField.text = ""
        passwordTextField.text = ""
    }
}
