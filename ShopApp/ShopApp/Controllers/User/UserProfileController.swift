//
//  UserProfileController.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 19/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

class UserProfileController: UIViewController, Alertable, TrackableMixin {

    let profileFactory = RequestFactory().makeProfileRequestFatory()
    
    @IBOutlet weak var profileName: UITextField!
    @IBOutlet weak var profilePassword: UITextField!
    @IBOutlet weak var profileEmail: UITextField!
    @IBOutlet weak var profileCreditCard: UITextField!
    @IBOutlet weak var profileBio: UITextView!
    
    var profileGender: String = Genders.MALE.rawValue
    
    @IBAction func userGenderButton(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
        case 0:
            profileGender = Genders.MALE.rawValue
        case 1:
            profileGender = Genders.FEMALE.rawValue
        default:
            break;
        }
    }
    
    @IBAction func saveButton(_ sender: Any) {
        updateUserProfile(profileId: Session.shared.user.id,
                          profileName: profileName.text!,
                          profilePassword: profilePassword.text!,
                          profileGender: profileGender,
                          profileEmail: profileEmail.text!,
                          profileCreditCard: profileCreditCard.text!,
                          profileBio: profileBio.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileName.text = Session.shared.user.login
        profileBio.text = "\(Session.shared.user.name) \(Session.shared.user.lastname)"
        profilePassword.isSecureTextEntry = true
    }

}
