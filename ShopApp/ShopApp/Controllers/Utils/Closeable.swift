//
//  Closeable.swift
//  ShopApp
//
//  Created by Оксана Лобышева on 01/06/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit
import Crashlytics

protocol Closeable { }

extension Closeable where Self: UIViewController {
    
    func logout(userId: Int){
        let authFactory = RequestFactory().makeAuthRequestFatory()
        if (userId > 0) {
            authFactory.logout(userId: userId) {response in
                switch response.result{
                case .success(_):
                    Session.shared.user = User(id: -1,
                                               login: Users.GUEST.rawValue,
                                               name: Users.GUEST.rawValue,
                                               lastname: Users.GUEST.rawValue)
                case .failure(let error):
                    print(Messages.ERROR.rawValue + error.localizedDescription)
                }
            }
        }
    }
}
