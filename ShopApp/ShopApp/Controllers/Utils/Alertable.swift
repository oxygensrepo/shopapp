//
//  Alertable.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 19/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit
import Crashlytics

protocol Alertable { }

extension Alertable where Self: UIViewController {
    
    func performAlert(title: String, message: String){
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Titles.OK.rawValue,
                                      style: .cancel,
                                      handler: nil))
        self.present(alert, animated: true)
    }
    
    func showAlertMsg(title: String, message: String, timeSeconds: Double){
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        self.present(alert, animated: true)
        delayWithSeconds(timeSeconds) {
            self.dismiss(animated: true, completion: {
                
            })
        }
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    func assertionFailure(_ message: String) {
        #if DEBUG
            Swift.assertionFailure(message)
        #else
            track(AnalyticsEvent.someMethod(
                Answers.logCustomEvent(
                    withName: "AssertionFailure",
                    customAttributes: ["message" : message])
                )
            )
        #endif
    }

}

