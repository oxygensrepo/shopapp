//
//  BasketCell.swift
//  ShopApp
//
//  Created by Оксана Лобышева on 26/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

class BasketCell: UITableViewCell {
    
   @IBOutlet weak var productName: UILabel!
   @IBOutlet weak var productPrice: UILabel!
   @IBOutlet weak var productQuantity: UILabel!
    
    public func configure(with basket: Basket) {
        productName.text = String(basket.productName)
        productPrice.text = String(basket.productPrice)
        productQuantity.text = Titles.QUANTITY.rawValue + String(basket.productQuantity)
    }

}
