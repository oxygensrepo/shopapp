//
//  BasketMethods.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 26/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

extension BasketController {
    
    func getBasket(userId: Int) {
        basketFactory.getBasket(userId: userId) {response in
            switch response.result {
                case .success(let data):
                    DispatchQueue.main.async{
                        self.basket = data.contents
                        self.productsCount = data.countGoods
                        self.productsSum = Int(data.amount)
                        self.tableView?.reloadData()
                        self.setTotalInfo()
                    }
                case .failure(let error):
                    self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
                }
        }
    }
    
    func removeFromBasket(product: Basket, position: Int) {
        basketFactory.removeFromBasket(productId: product.productId) {response in
            switch response.result {
                case .success(_):
                    DispatchQueue.main.async{
                        self.basket.remove(at: position)
                        self.productsCount -= UInt(product.productQuantity)
                        self.productsSum -= Int(product.productPrice) * product.productQuantity
                        self.tableView?.reloadData()
                        self.setTotalInfo()
                    }
                case .failure(let error):
                    self.assertionFailure(Messages.ERROR.rawValue + error.localizedDescription)
            }
        }
    }
    
    @objc func handlerRefresh(){
        getBasket(userId: Session.shared.user.id)
        refresher.endRefreshing()
    }
    
    func addRefresher(){
        refresher = UIRefreshControl()
        tableView.addSubview(refresher)
        refresher.attributedTitle = NSAttributedString(string: Messages.PULL_REFRESH.rawValue)
        refresher.tintColor = UIColor.purple
        refresher.addTarget(self, action: #selector(handlerRefresh), for: .valueChanged)
    }
    
    func setTotalInfo(){
        self.totalInfo.text = "Items: \(self.productsCount) Sum: \(self.productsSum) RUB"
    }
    
}
