//
//  BasketController.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 26/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit

class BasketController: UITableViewController, Alertable {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var totalInfo: UILabel!
    
    @IBAction func finishPayment(_ sender: Any) {
        if productsCount > 0 && productsSum > 0 {
            self.performAlert(title: Titles.CONFIRMATION.rawValue,
                              message: Messages.ORDER_CONFIRMATION.rawValue)
            basket = []
            productsCount = 0
            productsSum = 0
            tableView?.reloadData()
            setTotalInfo()
        }
    }
    
    let basketFactory = RequestFactory().makeBasketRequestFatory()
    var refresher: UIRefreshControl!
    var basket: [Basket] = []
    var productsCount: UInt = 0
    var productsSum: Int = 0   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBasket(userId: Session.shared.user.id)
        addRefresher()
        //navigationController?.setNavigationBarHidden(false, animated: true)
        setTotalInfo()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return basket.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellNames.basketCell.rawValue, for: indexPath) as! BasketCell
        cell.configure(with: basket[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete){
            removeFromBasket(product: basket[indexPath.row], position: indexPath.row)
        }
    }
    
}
