//
//  AppDelegate.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 18/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        return true;
    }

}

