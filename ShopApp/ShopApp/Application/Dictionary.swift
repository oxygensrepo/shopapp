//
//  Dictionary.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 17/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

enum SeguesId: String {
    case goToStore = "goToStore"
    case goToAuth = "goToAuth"
}

enum CellNames: String {
    case productCell = "ProductCell"
    case basketCell = "BasketCell"
}

enum AccessId: String {
    case loginView = "loginView"
    case catalogView = "catalogView"
}

enum Messages: String {
    case LOGIN_FAILED = "Invalid login or password"    
    case ERROR = "OOPS! Error: "
    case REGISTRATION_SUCCESS = "Welcome on board! \nNow you need to login"
    case DATA_INCOMPLETE = "Incomplete data for profile"
    case DATA_SAVED = "New data saved!"
    case REVIEW_APPROVED = "Review approved!"
    case ADD_BASKET_SUCCESS = "Product added to the basket!"
    case PULL_REFRESH = "Pull to refresh"
    case ORDER_CONFIRMATION = "Thank you for placing your order!"
}

enum Titles: String {
    case ERROR = "ERROR"
    case OK = "OK"
    case CONFIRMATION = "CONFIRMATION"
    case REGISTRATION_FORM = "Registration form"
    case USER_PROFILE_FORM = "User profile"
    case CATALOG = "Products catalog"
    case PRODUCT = "Product specification"
    case BASKET = "Your basket"
    case QUANTITY = "Quantity: "
}

enum Genders: String {
    case MALE = "m"
    case FEMALE = "f"
}

enum Users: String {
    case GUEST = "anonymous"
}
