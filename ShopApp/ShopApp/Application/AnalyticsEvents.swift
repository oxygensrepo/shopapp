//
//  AnalyticsEvents.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 31/05/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation
import Crashlytics

enum AnalyticsEvent {
    
    enum LoginParams {
        static let methodDefault = "login"
    }
    
    enum RegistrationParams {
        static let nameDefault = "registration"
        static let parameterDefault = "parameter"
        static let nameAssertionFailure = "assertionFailure"
    }
    
    enum UpdateProfileParams {
        static let nameDefault = "updateProfile"
    }
    
    case login(method: String, success: Bool)
    case registration(name: String, parameter: String)
    case updateProfile(name: String)
}

protocol TrackableMixin {
    func track(_ event: AnalyticsEvent)
}

extension TrackableMixin {
    func track(_ event: AnalyticsEvent) {
        switch event {
            case let .login(method, success):
                let success = NSNumber(value: success)
                Answers.logLogin(withMethod: method, success: success, customAttributes: nil)
            case let .registration(name, parameter):
                Answers.logCustomEvent(withName: name, customAttributes: ["parameter" : parameter])
            case let .updateProfile(name):
                Answers.logCustomEvent(withName: name)
            }
    }
}

