//
//  ShopAppUITests.swift
//  ShopAppUITests
//
//  Created by Oxana Lobysheva on 18/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import XCTest

class ShopAppUITests: XCTestCase {

    var app: XCUIApplication!
    let ERROR: String = "ERROR"
    let OK: String = "OK"
    let LOGIN: String = "LOGIN"
    let ERROR_MESSAGE: String = "Invalid login or password"

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }

    override func tearDown() {
    }

    func testAuthSuccess() {
        shapShotLoginScreen()
        enterAuthData(login: "admin", password: "123456")
        sleep(1)
        XCTAssertFalse(app.isDisplayingLoginView)
        shapShotProductsScreen()
    }
    
    func testAuthFail() {
        shapShotLoginScreen()
        enterAuthData(login: "nobody",
                      password: "wrongPassword")
        XCTAssert(isAlertPresent(expectedTitle: ERROR,
                                 expectedMessage: ERROR_MESSAGE) )
        app.alerts[ERROR].buttons[OK].tap()
        XCTAssertTrue(app.isDisplayingLoginView)
        shapShotLoginScreen()
    }
    
    func testCheckLogin() {
        shapShotLoginScreen()
        enterAuthData(login: "",
                      password: "123456")
        XCTAssert(isAlertPresent(expectedTitle: ERROR,
                                 expectedMessage: ERROR_MESSAGE) )
        app.alerts[ERROR].buttons[OK].tap()
        XCTAssertTrue(app.isDisplayingLoginView)
        shapShotLoginScreen()
    }
    
    func testCheckPassword() {
        shapShotLoginScreen()
        enterAuthData(login: "admin",
                      password: "")
        XCTAssert(isAlertPresent(expectedTitle: ERROR,
                                 expectedMessage: ERROR_MESSAGE) )
        app.alerts[ERROR].buttons[OK].tap()
        XCTAssertTrue(app.isDisplayingLoginView)
        shapShotLoginScreen()
    }
    
    func testCheckLoginAndPassword() {
        shapShotLoginScreen()
        enterAuthData(login: "",
                      password: "")
        XCTAssert(isAlertPresent(expectedTitle: ERROR,
                                 expectedMessage: ERROR_MESSAGE) )
        app.alerts[ERROR].buttons[OK].tap()
        XCTAssertTrue(app.isDisplayingLoginView)
        shapShotLoginScreen()
    }
    
    private func enterAuthData(login: String, password: String) {
        let scrollViewsQuery = app.scrollViews
        
        let loginTextField = scrollViewsQuery.textFields["Login"]
        loginTextField.tap()
        loginTextField.typeText(login)
        
        let passwordTextField = scrollViewsQuery.secureTextFields["Password"]
        passwordTextField.tap()
        passwordTextField.typeText(password)
        
        let button = scrollViewsQuery.buttons[LOGIN]
        button.tap()
    }
    
    private func isAlertPresent(expectedTitle: String, expectedMessage: String) -> Bool{
        let alert = app.alerts.element
        guard alert.exists else {
            return false
        }
        guard alert.staticTexts[expectedTitle].exists else {
            return false
        }
        guard alert.staticTexts[expectedMessage].exists else {
            return false
        }
        return true
    }
    
    private func shapShotLoginScreen(){
        snapshot("LoginScreen")
    }
    
    private func shapShotProductsScreen(){
        snapshot("ProductsScreen")
    }

}

extension XCUIApplication {
    
    var isDisplayingLoginView: Bool {
        return otherElements["loginView"].exists
    }

}
