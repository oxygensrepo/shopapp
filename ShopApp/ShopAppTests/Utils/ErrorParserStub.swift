//
//  ErrorParserStub.swift
//  ShopAppTests
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

public class ErrorParserStub: AbstractErrorParser {
    
    public func parse(_ result: Error) -> Error {
        return ApiErrorStub.fatalError as Error
    }
    
    public func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }
}
