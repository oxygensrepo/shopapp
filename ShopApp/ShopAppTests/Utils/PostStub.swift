//
//  PostStub.swift
//  ShopAppTests
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Foundation

public struct PostStub: Codable {
    let userId: Int
    let id: Int
    let title: String
    let body: String
}
