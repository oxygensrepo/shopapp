//
//  AuthTests.swift
//  ShopApp
//
//  Created by Oxana Lobysheva on 28/04/2019.
//  Copyright © 2019 Oxana Lobysheva. All rights reserved.
//

import Alamofire
import XCTest
@testable import ShopApp

class AuthTests: XCTestCase {

    var errorParser: ErrorParserStub!
    var auth = RequestFactory().makeAuthRequestFatory()
    
    override func setUp() {
        super.setUp()
        errorParser = ErrorParserStub()
    }
    
    override func tearDown() {
        super.tearDown()
        errorParser = nil
    }
    
    func test_auth_success() {
        auth.login(userName: "Somebody", password: "mypassword") { response in
            XCTAssert(response.result.isFailure, "Auth failed");
        }
    }
    
    func test_auth_wrongUserName() {
        auth.login(userName: "Nobody", password: "mypassword") { response in
            XCTAssert(response.result.isSuccess, "Auth success for wrong user name");
        }
    }

}
